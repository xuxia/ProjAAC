# DFS / BFS

Nous utilisons ici le graphe comme structure de données pour notre étude
 de l'algorithme DFS et de l'algorithme DFS. 

---
# Cas d'utilisation pour les tests

## Test 1

*Graph 1*

![Graph_test1](./img/graph_test1.svg)

**Résultats du programme**

![ResTest1](./img/RésultatTest1.jpg)


**Algo DFS(Ordre de passage théorique)**

![DFSTest1](./img/graph_test1_DFS.png)

**Algo BFS(Ordre de passage théorique)**

![BFSTest1](./img/graph_test1_BFS.png)

## Test 2

*Graph 2*

![Graph_test2](./img/graph_test2.svg)

**Résultats du programme**

![ResTest2](./img/RésultatTest2.jpg)


**Algo DFS(Ordre de passage théorique)**

![DFSTest1](./img/graph_test2_DFS.png)

**Algo BFS(Ordre de passage théorique)**

![BFSTest1](./img/graph_test2_BFS.png)