#include <stdio.h>
#include <stdlib.h>
#include "graph.h"
#include "algoBFS.h"
#include "algoDFS.h"

int main() {

 // Créer un graphe dirigé
    struct Graph* graph = createGraph(5);
    struct Graph* graph2 = createGraph(7);
    // Ajouter des arêtes dirigées
    // addEdge(graph, 0, 1);
    // addEdge(graph, 0, 2);
    // addEdge(graph, 1, 2);
    // addEdge(graph, 1, 4);
    // addEdge(graph, 2, 2);
    // addEdge(graph, 3, 0);
    // addEdge(graph, 3, 1);
    // addEdge(graph, 3, 4);
    // addEdge(graph, 4, 2);

    //test 1
    //créer un graph
    addEdge(graph, 0, 1);
    addEdge(graph, 0, 2);
    addEdge(graph, 0, 3);
    addEdge(graph, 1, 4);
    addEdge(graph, 3, 5);
    addEdge(graph, 4, 6);
    addEdge(graph, 6, 5);

    // Afficher le graphe
    printGraph(graph);
    printf("\nParcours DFS à partir du sommet 0 : ");
    DFS(graph, 0);
    printf("\n");

    printf("\nParcours BFS à partir du sommet 0 : ");
    BFS(graph, 0);
    printf("\n");

    //test 2
    addEdge(graph2, 0, 1);
    addEdge(graph2, 0, 3);
    addEdge(graph2, 1, 3);
    addEdge(graph2, 1, 2);
    addEdge(graph2, 2, 4);
    addEdge(graph2, 3, 2);
    addEdge(graph2, 3, 4);
    // Afficher le graphe
    printGraph(graph2);
    printf("\nParcours DFS à partir du sommet 0 : ");
    DFS(graph2, 0);
    printf("\n");

    printf("\nParcours BFS à partir du sommet 0 : ");
    BFS(graph2, 0);
    printf("\n");
    return 0;


    return 0;



}