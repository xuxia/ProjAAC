// La fonction de parcours en largeur (Breadth-First Search) commence à
// un noeud racine, et parcours tous ses fils, puis tous les fils non explorés
// des fils, et ainsi de suite.
// Pour ce faire, on créer un tableau visited[] de la taille du graphe (en noeuds)
// qui retient quel noeud a déjà été visité. Ensuite, pour savoir quel noeud 
// visiter prochainement, nous avons recours à une file. Quand un noeud est extrait
// de la file, on ajoute ses fils non visités à la file et on parcourt le prochain dans la file.


// Fonction BFS :
// Entrée: struct Graph* graph, 
//         int startVertex     (racine de départ du parcours)
        
// Sortie: void    (affichage sur le terminal du parcours)

// Fonction de parcours en largeur (BFS)
void BFS(struct Graph* graph, int startVertex) {
    // Tableau pour suivre les noeuds visités
    int* visited = (int*)malloc(graph->vertices * sizeof(int));
    for (int i = 0; i < graph->vertices; i++) {
        visited[i] = 0; // Marquer tous les noeuds comme non visités
    }

    // File de traitement pour BFS initialisée
    int* queue = (int*)malloc(graph->vertices * sizeof(int));
    int front = 0, rear = 0;

    // Marquer le noeud de départ comme visité et l'ajouter à la file
    visited[startVertex] = 1;
    queue[rear++] = startVertex;

    // Tant que la file n'est pas vide
    while (front != rear) {
        // Extraire un noeud de la file et l'afficher, 
        // et incrémenté la tête de file
        int currentVertex = queue[front++];
        printf("%d ", currentVertex);

        // Parcourir tous les noeuds adjacents non visités
        // et les ajouter dans la file, incrémentant la fin
        struct Node* current = graph->array[currentVertex].head;
        while (current != NULL) {
            if (!visited[current->valeur]) {
                visited[current->valeur] = 1;
                queue[rear++] = current->valeur;
            }
            current = current->next;
        }
    }

    free(visited);
    free(queue);
}


// Un désavantage de l'utilisation de la file : int* queue = (int*)malloc(graph->vertices * sizeof(int));
// est que sa taille équivaut à la taille du graphe mais qu'elle ne sera jamais remplie, même dans le cas
// où il existe une racine, et tous les autres noeuds sont ses fils (la file sera remplie à N-1, car la
// racine a été extraite)

// NB : Ce parcours fonctionne pour un graphe orienté et non-orienté