// La fonction de parcours en profondeur (Depth-First Search) commence à
// un noeud racine, et parcours un fils, puis un successeur de ce dernier, 
// jusqu'à un cul-de-sac, puis reprend le parcoursavec les autres fils de la racine
// Pour ce faire, on utilise un tableau visited[] de la taille du graphe (en noeuds)
// qui retient quel noeud a déjà été visité. Ensuite, pour savoir quel noeud 
// visiter prochainement, en partant d'un noeud, on choisi un de ses fils non visités. 
// On le marque comme visité, et on le parcours en réitérant la procédure. On fait ceci 
// pour tous les noeuds en partant de la racine, donc on a besoin d'une fonction récursive.


// Fonction DFSUtil :
// Entrée: struct Graph* graph, 
//         int startVertex     (racine de départ du parcours)
//         int visited[]
        
// Sortie: void    (affichage sur le terminal du parcours)

// Cette fonction est le coeur du calcul, elle est récursive et permet de parcours
// tous les noeuds en partant d'une racine
void DFSUtil(struct Graph* graph, int vertex, int visited[]) {
    // Marquer le noeud actuel comme visité
    visited[vertex] = 1;
    printf("%d ", vertex);

    // Parcourir tous les noeuds adjacents non visités
    struct Node* current = graph->array[vertex].head;
    while (current != NULL) {
        if (!visited[current->valeur]) {
            DFSUtil(graph, current->valeur, visited);
        }
        current = current->next;
    }
}

// Fonction mère pour effectuer le parcours DFS
void DFS(struct Graph* graph, int startVertex) {
    // Tableau pour suivre les noeuds visités
    int* visited = (int*)malloc(graph->vertices * sizeof(int));

    // Initialiser tous les noeuds comme non visités
    for (int i = 0; i < graph->vertices; ++i) {
        visited[i] = 0;
    }

    // Appeler la fonction utilitaire DFS, qui parcours le graphe
    // et affiche le résultat dans le terminal
    DFSUtil(graph, startVertex, visited);

    // Libérer la mémoire allouée pour le tableau visited
    free(visited);
}


// NB : Ce parcours fonctionne pour un graphe orienté et non-orienté