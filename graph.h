// Structure d'un nœud dans la liste d'adjacence
struct Node {
    int valeur;
    struct Node* next;
};

// Structure d'une liste d'adjacence
struct AdjList {
    struct Node* head;
};

// Structure du graphe
struct Graph {
    int vertices;
    struct AdjList* array;
};

// Fonction pour créer un nouveau nœud
struct Node* createNode(int valeur) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    newNode->valeur = valeur;
    newNode->next = NULL;
    return newNode;
}

// Fonction pour créer un graphe avec V sommets
struct Graph* createGraph(int V) {
    struct Graph* graph = (struct Graph*)malloc(sizeof(struct Graph));
    graph->vertices = V;

    // Créer un tableau de listes d'adjacence
    graph->array = (struct AdjList*)malloc(V * sizeof(struct AdjList));

    // Initialiser chaque liste d'adjacence comme vide
    for (int i = 0; i < V; ++i) {
        graph->array[i].head = NULL;
    }

    return graph;
}

// Fonction pour ajouter une arête dirigée dans le graphe
void addEdge(struct Graph* graph, int from, int to) {
    // Créer un nouveau nœud avec la destination 'to'
    struct Node* newNode = createNode(to);

    // Ajouter 'to' à la liste d'adjacence de 'from'
    newNode->next = graph->array[from].head;
    graph->array[from].head = newNode;
}

// Fonction pour ajouter une arête non-dirigée dans le graphe
void addEdgeBothWays(struct Graph* graph, int from, int to) {
    // Créer un nouveau nœud avec la destination 'to'
    addEdge(graph, from, to);
    addEdge(graph, to, from);
}

// Fonction pour afficher le graphe(Liste d'adjacence)
void printGraph(struct Graph* graph) {
    for (int i = 0; i < graph->vertices; ++i) {
        struct Node* current = graph->array[i].head;
        printf("Liste d'adjacence du sommet %d : ", i);
        while (current != NULL) {
            printf("%d -> ", current->valeur);
            current = current->next;
        }
        printf("NULL\n");
    }
}
